import entity.Farmer;
import entity.Order;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;

import java.util.List;

@RunWith(MockitoJUnitRunner.class)
public class FarmServiceTest {

    @Mock
    private List<Order> orders;

    @Mock
    private Farmer farmer;

    private FarmService underTest;

    @Before
    public void setUp() {
        underTest = new FarmService(orders,farmer);
    }

    @Test
    public void t1 () {
        Mockito.when(farmer.getWallet()).thenReturn(100);
        Assert.assertEquals(100,underTest.getFarmerWallet());
    }
}
