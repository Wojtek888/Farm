import entity.Materials;

import java.util.InputMismatchException;

class FarmController {

    private ScannerController scannerController;
    private FarmService farmService;

    public FarmController(ScannerController scannerController, FarmService farmService) {
        this.scannerController = scannerController;
        this.farmService = farmService;
    }

    public FarmController() {
        this.farmService = new FarmService();
        scannerController = new ScannerControllerImpl();
    }


    public void start() {
        System.out.println("Witaj w programie Farma");
        secondMenu();
    }

    private void showMenu() {
        System.out.println("Menu");
        System.out.println("1. Sprzedaj");
        System.out.println("2. Pokaz stan portfela");
        System.out.println("3. Aktualny stan spichlerza");
        System.out.println("4. Dodaj produkty");
        System.out.println("5. Lista produktow wymagajacych uzupelnienia");
        System.out.println("6. Zamknij farme");
    }

    private void secondMenu() {
        String menuOption = "0";

        while (!menuOption.equals("6")) {
            showMenu();
            System.out.println("Wybierz opcje");
            menuOption = scannerController.next();
            switch (menuOption) {
                case "1": {
                    farmService.uploadOrders();
                    boolean orderLogic = true;
                    while (orderLogic) {
                        if(farmService.getOrdersSize() == 0) break;
                        farmService.showAllOrders();
                        farmService.checkEndingProducts();
                        int orderNr = getOrderNr();
                        orderLogic = farmService.orderLogic(orderNr);
                    }
                    break;
                }
                case "2": {
                    System.out.println("Stan portfela: " + farmService.getFarmerWallet());
                    break;
                }
                case "3": {
                    farmService.granaryActualStatus();
                    break;
                }
                case "4": {
                    addMaterialsMenu();
                    break;
                }
                case "5": {
                    if(printListOfProductToUpdate()) {
                        int orderNr = getOrderNr();
                        if(orderNr <= 0 || orderNr > getSizeOfUpdateMaterialList() + 1) {
                            System.out.println("Podana zla wartosc");
                            break;
                        }
                        if(orderNr != getSizeOfUpdateMaterialList() + 1) {
                            int quantity = getQuantity();
                            farmService.materialToUpdate(orderNr,quantity);
                        }
                    }
                    break;
                }
                case "6": {
                    System.out.println("Zamkniecie farmy");
                    farmService.closeFarm();
                    break;
                }
                default: {
                    System.out.println("Podano zla wartosc");
                    break;
                }
            }
        }
    }

    private void addMaterialsMenu() {
        System.out.println("1. Dodaj ser");
        System.out.println("2. Dodaj mleko");
        System.out.println("3. Dodaj jajka");
        System.out.println("4. Cofnij");
        System.out.println("Wybierz opcje");
        String menuOption = scannerController.next();
        farmService.addMaterialChoice(menuOption);
    }


    private int getOrderNr() {
        System.out.println("\nStan portfela: " + farmService.getFarmerWallet());
        System.out.println("Wybierz opcje");
        int orderNr;
        while(true) {
            try {
                orderNr = scannerController.nextInt();
                break;
            } catch(InputMismatchException e) {
                System.out.println("Podano zla wartosc. Podaj ponownie");
            }
        }
       return orderNr;
    }

    private int getQuantity() {
        System.out.println("Podaj ilosc produktu do uzupelnienia");
        int quantity;
        while(true) {
            try {
                quantity = scannerController.nextInt();
                break;
            } catch(InputMismatchException e) {
                System.out.println("Podano zla wartosc. Podaj ponownie");
            }
        }
        return quantity;
    }

    private boolean printListOfProductToUpdate() {
        int i = 1;
        if(getSizeOfUpdateMaterialList() == 0) {
            System.out.println("Lista aktualnie jest pusta");
            return false;
        }
        for (Materials material : farmService.listOfEndingProducts()) {
            System.out.println(i++ + ". " + material);
        }
        System.out.println(getSizeOfUpdateMaterialList() + 1 + ". Cofnij");
        return true;
    }

    private int getSizeOfUpdateMaterialList() {
        return farmService.listOfEndingProducts().size();
    }

}
