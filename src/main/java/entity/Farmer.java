package entity;

import grenary.GranaryRepository;

import java.util.List;
import java.util.Map;

public class Farmer {
    /**
     * Trough this class we are able to check condition of our wallet and materials in granary
     */

    private int wallet;
    private GranaryRepository granary;

    public Farmer(int wallet, GranaryRepository granary) {
        this.wallet = wallet;
        this.granary = granary;
    }

    public int getWallet() {
        return wallet;
    }

    public void setWallet(int wallet) {
        this.wallet = wallet;
    }

    public Map<Materials, Integer> getGranary() {
        return granary.getGranary();
    }

    public void addMaterialToGranary(Materials materials, int quantity) {
        granary.addMaterialToGranary(materials,quantity);
    }

    public List<Materials> getMaterialsQuantityLess20() {
        return granary.getMaterialsQuantityLess20();
    }

}
