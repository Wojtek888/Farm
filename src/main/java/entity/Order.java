package entity;


public class Order {

    private Wholesaler wholesaler;
    public Materials material;
    private int quantity;

    /**
     * Method which show wholesaler's order
     */

    public Order(Wholesaler wholesaler, Materials material, int quantity) {
        this.wholesaler = wholesaler;
        this.material = material;
        this.quantity = quantity;
    }

    public Wholesaler getWholesaler() {
        return wholesaler;
    }

    public int getQuantity() {
        return quantity;
    }

    public Materials getMaterial() {
        return material;
    }

    @Override
    public String toString() {
        return "Zamowienie -> " +
                "Hurtownia  " + wholesaler.getName() +
                ", produkt  " + material.getName() +
                ", ilosc = " + quantity + " sztuk";
    }
}
