package entity;

public enum Wholesaler {
    /**
     * List of all wholesalers
     */
    WHOLESALER_SUN("Sun"),
    WHOLESALER_FLOWER("Flower"),
    LIDL("Lidl");

    private String name;

    Wholesaler(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }
}


