package entity;

public enum Materials {
    /**
     * List of all materials which are in our granary
     */
    WHEAL("Wheal", 5),
    RYE("Rye", 4),
    STRAWBERRY("Strawberry", 7),
    WATERMELON("Watermelon", 10),
    CARROT("Carrot", 5),
    CUCUMBER("Cucumber", 5),
    TOMATO("Tomato", 5),
    LETTUCE("Lettuce", 5),
    CABBAGE("Cabbage", 9),
    MEAT("Meat", 20),
    MILK("Milk", 8),
    EGGS("Eggs", 12),
    CHEESE("Cheese", 18);

    Materials(String name, int price) {
        this.name = name;
        this.price = price;
    }

    private String name;
    private int price;

    public String getName() {
        return name;
    }

    public int getPrice() {
        return price;
    }

}
