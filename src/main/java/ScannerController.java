public interface ScannerController {

    public String next();
    public int nextInt();
}
