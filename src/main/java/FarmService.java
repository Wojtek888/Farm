import entity.Farmer;
import entity.Materials;
import entity.Order;
import entity.Wholesaler;
import grenary.GranaryRepositoryImpl;

import java.util.*;

/**
 * FarmService contains farm logic
 *
 * @author Group project :)
 */

public class FarmService {

    /**
     * List of all orders
     */
    private List<Order> orders;
    /**
     * Farmer
     */
    private Farmer farmer;
    /**
     * Repository of all materials
     */

    public FarmService(List<Order> orders, Farmer farmer) {
        this.orders = orders;
        this.farmer = farmer;
    }

    public FarmService() {
        this.farmer = new Farmer(100000, new GranaryRepositoryImpl());
        this.orders = new ArrayList<>();
    }

    /**
     * <p> Method returns set of materials from farmer storage. </p>
     *
     * @return the set materials
     */

    private Set<Materials> getMaterialsFromFarmer() {
        return farmer.getGranary().keySet();
    }

    /**
     * <p> Method to randomize orders list </p>
     */

    private void randomizeOrders() {
        int ordersNumber = randomizeOrdersNr();
        for (int i = 0; i < ordersNumber; i++) {
            orders.add(new Order(randomizeWholesSaler(), randomizeMaterial(), randomizeQuantity()));
        }
    }

    /**
     * <p> Method to get the order list
     * </p>
     * @return list of orders
     */

    private List<Order> getOrders() {
        return orders;
    }

    /**
     * <p> Method that prints all orders list. If the order is possible to perform,
     *     symbol [V] appears at the end of the line, if not symbol changes to [X]
     * </p>
     */

    public void showAllOrders() {
        int orderNumber = 1;
        for (Order order : orders) {
            if (order.getQuantity() > farmer.getGranary().get(order.material)) {
                System.out.println(orderNumber++ + ". " + order + " [X]");
            } else {
                System.out.println(orderNumber++ + ". " + order + " [V]");
            }
        }
        System.out.println(orders.size() + 1 + ". Cofnij");
    }

    /**
     * <p> Removes order from list of orders </p>
     * @param index the index of order to remove
     */

    private void removeOrder(int index) {
        orders.remove(index);
    }

    /**
     * <p> Method to random choose material from list of materials </p>
     *
     * @return random material from list
     */

    private Materials randomizeMaterial() {
        ArrayList<Materials> materials = new ArrayList<>(getMaterialsFromFarmer());
        return materials.get(new Random().nextInt(getMaterialsFromFarmer().size()));
    }

    /**
     * <p> Method to random choose wholesaler from list of wholesalers </p>
     *
     * @return random wholesaler from list
     */

    private Wholesaler randomizeWholesSaler() {
        return Wholesaler.values()[new Random().nextInt(Wholesaler.values().length)];
    }

    /**
     * <p> Method return random quantity number from 1 - 100 </p>
     *
     * @return random quantity number
     */

    private int randomizeQuantity() {
        return new Random().nextInt(100) + 1;
    }

    /**
     * <p> Method return random order number from 3 - 10 </p>
     *
     * @return random order number
     */

    private int randomizeOrdersNr() {
        return new Random().nextInt((10 - 3) + 1) + 3;
    }

    /**
     * <p> Method adds material to granary repository </p>
     * @param material the material to add
     */

    private void addMaterialToGranary(Materials material) {

        farmer.addMaterialToGranary(material, 100);
    }


    /**
     * <p> Method to set to farmer wallet value </p>
     * @param value the value to set
     */

    private void setFarmerWallet(int value) {
        farmer.setWallet(value);
    }

    /**
     * <p> Method to get farmer wallet value </p>
     *
     * @return random value of farmers wallet
     */

    int getFarmerWallet() {
        return farmer.getWallet();
    }

    /**
     * <p> Method contains order realize logic. It prints "Podano zla wartosc" if the orderNr is out of scope.
     *     If not it take name and quantity from chosen order and prints name of product, quantity and cost
     *     of the whole order. At the end it checks if the order is correct or not and prints appropriate message
     * </p>
     *
     * @param orderNr value of order number that user chooses.
     */

    private void realizeOrder(int orderNr) {

        if(orderNr < 1 || orderNr > orders.size()) {
            System.out.println("Podano zla wartosc");
            return;
        }
        Materials orderMaterial = orders.get(orderNr - 1).getMaterial();
        int orderQuantity = orders.get(orderNr - 1).getQuantity();
        int orderPrice = orders.get(orderNr - 1).getMaterial().getPrice() * orders.get(orderNr - 1).getQuantity();
        System.out.println("Produkt: " + orderMaterial.getName()
                + ", ilosc: " + orderQuantity
                + ", koszt zamowienia: " + orderPrice);
        if (isOrderPossible(orderMaterial, orderQuantity, orderPrice)) {
            System.out.println("Zamowienie sie powiodlo\n");
            removeOrder(orderNr - 1);
        } else {
            System.out.println("Zamowienie sie nie powiodlo\n W spichelrzu nie ma wystarczajacej ilosci produktu, aby zrealizowac zamowienie");
        }
    }

    /**
     * <p> A method to check whether the order is possible to perform.
     *     It compares the order price with farmer wallet, material quantity in granary
     *     with order quantity and returns true or false.
     * </p>
     *
     * @param material material to check in granary
     * @param orderQuantity order quantity
     * @param orderPrice full price of order
     * @return true if order is possible to do, or false if not
     */

    private boolean isOrderPossible(Materials material, int orderQuantity, int orderPrice) {
        if (orderPrice <= getFarmerWallet() && farmer.getGranary().get(material) >= orderQuantity) {
            farmer.getGranary().put(material, farmer.getGranary().get(material) - orderQuantity);
            setFarmerWallet(getFarmerWallet() + orderPrice);
            return true;
        }
        return false;
    }

    /**
     * <p> A method that allows to add additional materials (Cheese, milk, eggs)
     *     to granary depends on user choice.
     *     If user check eg. cheese, price 10000 is equals price 100 quantity of cheese and
     *     cost to open granary.
     * </p>
     *
     * @param menuOption material that user inputs
     */

    void addMaterialChoice(String menuOption) {
        switch (menuOption) {
            case "1": {
                if (checkWalletForNewMaterials(10000)) {
                    addMaterialsLogic(10000, Materials.CHEESE);
                }
                break;
            }
            case "2": {
                if (checkWalletForNewMaterials(20000)) {
                    addMaterialsLogic(20000, Materials.MILK);
                }
                break;
            }
            case "3": {
                if (checkWalletForNewMaterials(50000)) {
                    addMaterialsLogic(50000, Materials.EGGS);
                }
                break;
            }
            case "4": {
                break;
            }
            default: {
                System.out.println("Podano zla wartosc");
                break;
            }
        }
    }

    /**
     * <p> Method sets all of the products in farmer granary to 0.
     *     It imitates the destocking of the materials in granary
     * </p>
     *
     */

    void closeFarm() {
        for (Map.Entry<Materials, Integer> entry : farmer.getGranary().entrySet()) {
            entry.setValue(0);
            System.out.println("Likwidacja " + entry.getKey().getName() + " stan zasobow: " + entry.getValue());
        }
    }

    /**
     * <p> Method checks if it is possible to add new material
     *     depends on the farmers wallet
     * </p>
     * @param price price value
     * @return true if farmer has money for new material
     */

    private boolean checkWalletForNewMaterials(int price) {
        if(getFarmerWallet() >= price) return true;
        System.out.println("Nie masz wystarczajacej ilosci srodkow, aby kupic dodatkowe produkty");
        return false;
    }

    /**
     * <p> Method checks if material that user want to add is currently in the granary
     * </p>
     * @param price price value
     * @param material material to check
     */

    private void addMaterialsLogic(int price, Materials material) {
        if(getMaterialsFromFarmer().contains(material)) {
            System.out.println("Dany material aktualnie znajduje sie w spichlerzu");
            return;
        }
        setFarmerWallet(getFarmerWallet() - price);
        addMaterialToGranary(material);
    }

    /**
     * Method that adds the ending product to the granary
     * quantity given by user, price 20% lower than the product price for the wholesaler
     * @param materialNr product number of list
     * @param materialQuantity quantity given by user
     */


    public void materialToUpdate(int materialNr, int materialQuantity) {
        Materials materialToUpdate = listOfEndingProducts().get(materialNr-1);
        int wholeSalePrice = (int) (materialToUpdate.getPrice()* 0.8);
        int orderPrice = materialQuantity * wholeSalePrice;
        if(farmer.getGranary().containsKey(materialToUpdate) && orderPrice <= farmer.getWallet() && orderPrice > 0) {
            farmer.addMaterialToGranary(materialToUpdate,farmer.getGranary().get(materialToUpdate) + materialQuantity);
            farmer.setWallet(farmer.getWallet() - orderPrice);
            System.out.println("Dodano " + materialToUpdate + " do spichlerza, w ilosci: " + materialQuantity + " za kwote " + orderPrice);
        } else {
            System.out.println("Operacja sie nie udala, bledne dane");
        }
    }

    /**
     * List of products which is less than 20 in the granary
     */

    public List<Materials> listOfEndingProducts() {
        return farmer.getMaterialsQuantityLess20();
    }

    /**
     * <p> Method prints granary actual quantity status
     * </p>
     */

    void granaryActualStatus() {
        for (Map.Entry<Materials, Integer> entry : farmer.getGranary().entrySet()) {
            System.out.println(entry.getKey().getName() + " Stan zasobow: " + entry.getValue());
        }
    }

    /**
     * <p> Method checks if user want to go back to the option menu,
     *     or choose order
     * </p>
     * @param orderNr orderNr value that user input
     * @return true if choose the correct order, or false if user wants to go back to menu
     */

    boolean orderLogic(int orderNr) {
        if (orderNr == getOrders().size() + 1) {
            return false;
        }
        realizeOrder(orderNr);
        return true;
        }

        /**
         * <p> Method that uploads the orders list with new randomize orders
         * </p>
         */

    void uploadOrders() {
        getOrders().clear();
        randomizeOrders();
    }

    /**
     * <p> Method to get the order list size
     * </p>
     * @return orders list size
     */

    public int getOrdersSize() {
        return orders.size();
    }


    /**
     * Method check if quantity of product is less than 20,
     * if is's less than 20, it shows the name of materials to fill in granary, if you want
     */


    public void checkEndingProducts() {
        if (listOfEndingProducts().size() > 0) {
            System.out.println("Produkty wymagajace uzupelnienia: ");
            for (Materials materials : listOfEndingProducts()) {
                System.out.print(materials.getName() + ", ");
            }
            System.out.println("\nW celu uzupelnienia produktu przejdz do punktu 5 w menu");
        }
    }
}
