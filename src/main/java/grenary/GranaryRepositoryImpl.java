package grenary;

import entity.Materials;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

public class GranaryRepositoryImpl implements GranaryRepository {

    private static Map<Materials, Integer> granary = new HashMap<>();

    static {
        granary.put(Materials.CABBAGE, 100);
        granary.put(Materials.CARROT, 120);
        granary.put(Materials.CUCUMBER, 90);
        granary.put(Materials.LETTUCE, 70);
        granary.put(Materials.MEAT, 200);
        granary.put(Materials.RYE, 80);
        granary.put(Materials.STRAWBERRY, 150);
        granary.put(Materials.TOMATO, 100);
        granary.put(Materials.WATERMELON, 180);
        granary.put(Materials.WHEAL, 60);
    }

    @Override
    public void addMaterialToGranary(Materials materials, int quantity) {
        granary.put(materials, quantity);
    }

    @Override
    public Map<Materials, Integer> getGranary() {
        return granary;
    }

    /**
     * In our project we can add materials to granary if quantity is less than 20
     * @return
     */

    @Override
    public List<Materials> getMaterialsQuantityLess20() {
        return  granary.entrySet().stream().filter(x -> (x.getValue() < 20)).map(Map.Entry::getKey).collect(Collectors.toList());
    }

}
