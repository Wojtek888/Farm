package grenary;

import entity.Materials;

import java.util.List;
import java.util.Map;

public interface GranaryRepository {

    void addMaterialToGranary(Materials materials, int quantity);

    Map<Materials, Integer> getGranary();

    List<Materials> getMaterialsQuantityLess20();

}
