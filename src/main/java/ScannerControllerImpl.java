import java.util.Scanner;

public class ScannerControllerImpl implements ScannerController {

    @Override
    public String next() {
        Scanner scanner = new Scanner(System.in);
        return scanner.next();
    }

    @Override
    public int nextInt() {
        Scanner scanner = new Scanner(System.in);
        return scanner.nextInt();
    }
}

