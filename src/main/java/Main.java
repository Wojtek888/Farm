public class Main {
    public static void main(String[] args) {
/**
 * Main method to start "Farmer Project"
 */
        FarmController farmController = new FarmController();
        farmController.start();
    }
}
